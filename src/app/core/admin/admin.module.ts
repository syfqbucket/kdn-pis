import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { 
  BsDropdownModule, 
  ProgressbarModule, 
  TooltipModule, 
  BsDatepickerModule
} from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';

import { FusionChartsModule } from 'angular-fusioncharts';
import * as FusionCharts from 'fusioncharts';
import * as Charts from 'fusioncharts/fusioncharts.charts';
import * as Widgets from 'fusioncharts/fusioncharts.widgets';
import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import * as PowerChart from 'fusioncharts/fusioncharts.powercharts';  
import * as HCWidgets from 'fusioncharts/fusioncharts.widgets'
FusionChartsModule.fcRoot(
  FusionCharts, 
  Charts,
  Widgets,
  FusionTheme,
  PowerChart,
  HCWidgets
);

import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

import { AdminRoutes } from './admin.routing';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ToolsComponent } from './tools/tools.component';
import { CrimeComponent } from './crime/crime.component';
import { HotspotComponent } from './hotspot/hotspot.component';
import { TrendsComponent } from './trends/trends.component';
import { ScenarioOneComponent } from './scenario-one/scenario-one.component';
import { ScenarioTwoComponent } from './scenario-two/scenario-two.component';
import { ReportComponent } from './report/report.component';
import { AdministratorComponent } from './administrator/administrator.component';
import { AuditComponent } from './audit/audit.component';
import { AnalyticsComponent } from './analytics/analytics.component';

@NgModule({
  declarations: [
    DashboardComponent,
    ToolsComponent,
    CrimeComponent,
    HotspotComponent,
    TrendsComponent,
    ScenarioOneComponent,
    ScenarioTwoComponent,
    ReportComponent,
    AdministratorComponent,
    AuditComponent,
    AnalyticsComponent
  ],
  imports: [
    CommonModule,
    BsDropdownModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot(),
    RouterModule.forChild(AdminRoutes),
    FusionChartsModule,
    NgxMapboxGLModule.withConfig({
      accessToken: 'pk.eyJ1Ijoid3lra3NzIiwiYSI6ImNqMjR6aTdmdzAwNHMzMnBvbjBucjlqNm8ifQ.6GjGpofWBVaIuSnhdXQb5w', // Optionnal, can also be set per map (accessToken input of mgl-map)
      //geocoderAccessToken: 'TOKEN' // Optionnal, specify if different from the map access token, can also be set per mgl-geocoder (accessToken input of mgl-geocoder)
    }),
    BsDatepickerModule.forRoot()
  ]
})
export class AdminModule { }
