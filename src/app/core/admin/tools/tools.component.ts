import { Component, OnInit } from '@angular/core';
import { Map, SymbolLayout } from 'mapbox-gl';

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss']
})
export class ToolsComponent implements OnInit {

  mapboxStyle = 'mapbox://styles/mapbox/dark-v9'
  mapboxZoom = 15.5
  mapboxCenter = [101.7136,3.1489]
  mapboxPitch = 45
  mapboxBearing = -17.6

  mapboxLayerFilter = ['==', 'extrude', 'true']
  mapboxLayerMinZoom = 15
  mapboxLayerPaint = {
    'fill-extrusion-color': '#aaa',
    'fill-extrusion-height': [
      'interpolate',
      ['linear'],
      ['zoom'],
      15, 0,
      15.05,
      ['get', 'height']
    ],
    'fill-extrusion-base': [
      'interpolate',
      ['linear'],
      ['zoom'],
      15, 0,
      15.05,
      [
        'get',
        'min_height'
      ]
    ],
    'fill-extrusion-opacity': .6
  }

  labelLayerId: string;


  onLoad(mapInstance: Map) {
    const layers = mapInstance.getStyle().layers!;

    for (let i = 0; i < layers.length; i++) {
      if (layers[i].type === 'symbol' && (<SymbolLayout>layers[i].layout)['text-field']) {
        this.labelLayerId = layers[i].id;
        break;
      }
    }
  }

  constructor() { }

  ngOnInit() {
  }

}
