import { Component, OnInit } from '@angular/core';
import * as hotspots from './hotspot.json';
import { Layer } from 'mapbox-gl';
import * as earthquakes from './earthquakes.geo.json';

/*const layersData: [number, string][] = [
  [0, 'green'],
  [3, 'orange'],
  [5, 'red']
];*/

@Component({
  selector: 'app-crime',
  templateUrl: './crime.component.html',
  styleUrls: ['./crime.component.scss']
})
export class CrimeComponent implements OnInit {

  earthquakes: object;

  clusterLayers: Layer[];

  async ngOnInit() {
    this.earthquakes = await import('./hotspot.json');
    const layersData: [number, string][] = [
      [0, 'green'],
      [3, 'orange'],
      [5, 'red']
    ];
    this.clusterLayers = layersData.map((data, index) => ({
      id: `cluster-${index}`,
      paint: {
        'circle-color': data[1],
        'circle-radius': 70,
        'circle-blur': 1
      },
      filter: index === layersData.length - 1 ?
        ['>=', 'point_count', data[0]] :
        ['all',
          ['>=', 'point_count', data[0]],
          ['<', 'point_count', layersData[index + 1][0]]
        ]
    }));
  }

  constructor() { }


}
