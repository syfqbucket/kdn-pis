import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hotspot',
  templateUrl: './hotspot.component.html',
  styleUrls: ['./hotspot.component.scss']
})
export class HotspotComponent implements OnInit {

  earthquakes: object;

  async ngOnInit() {
    this.earthquakes = await import('../crime/hotspot.json');
  }

}
