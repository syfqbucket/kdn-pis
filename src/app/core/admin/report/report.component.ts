import { Component, OnInit } from '@angular/core';

const data = {
  chart: {
    drawcrossline: "1",
    plottooltext: "$seriesName $dataValue in $label",
    theme: "fusion"
  },
  categories: [
    {
      category: [
        {
          label: "Jan"
        },
        {
          label: "Feb"
        },
        {
          label: "Mar"
        },
        {
          label: "Apr"
        },
        {
          label: "May"
        },
        {
          label: "Jun"
        },
        {
          label: "Jul"
        },
        {
          label: "Aug"
        },
        {
          label: "Sep"
        },
        {
          label: "Oct"
        },
        {
          label: "Nov"
        },
        {
          label: "Dec"
        }
      ]
    }
  ],
  dataset: [
    {
      seriesname: "Add",
      data: [
        {
          value: "920"
        },
        {
          value: "790"
        },
        {
          value: "750"
        },
        {
          value: "817"
        },
        {
          value: "662"
        },
        {
          value: "681"
        },
        {
          value: "726"
        },
        {
          value: "791"
        },
        {
          value: "872"
        },
        {
          value: "621"
        },
        {
          value: "625"
        },
        {
          value: "550"
        }
      ]
    },
    {
      seriesname: "Modify",
      data: [
        {
          value: "322"
        },
        {
          value: "521"
        },
        {
          value: "412"
        },
        {
          value: "521"
        },
        {
          value: "532"
        },
        {
          value: "124"
        },
        {
          value: "343"
        },
        {
          value: "532"
        },
        {
          value: "621"
        },
        {
          value: "316"
        },
        {
          value: "398"
        },
        {
          value: "542"
        }
      ]
    },
    {
      seriesname: "Delete",
      data: [
        {
          value: "122"
        },
        {
          value: "121"
        },
        {
          value: "212"
        },
        {
          value: "171"
        },
        {
          value: "232"
        },
        {
          value: "124"
        },
        {
          value: "143"
        },
        {
          value: "332"
        },
        {
          value: "321"
        },
        {
          value: "216"
        },
        {
          value: "198"
        },
        {
          value: "242"
        }
      ]
    }
  ]
};

const data2 = {
  chart: {
    showpercentvalues: "1",
    aligncaptionwithcanvas: "0",
    captionpadding: "0",
    decimals: "1",
    plottooltext:
      "<b>$percentValue</b> <b>$label</b>",
    centerlabel: "$value",
    pieradius: "90",
    theme: "fusion"
  },
  data: [
    {
      label: "Add",
      value: "1000"
    },
    {
      label: "Modify",
      value: "5300"
    },
    {
      label: "Delete",
      value: "10500"
    }
  ]
};

const data3 = {
  chart: {
    showlegend: "1",
    showpercentvalues: "1",
    legendposition: "bottom",
    usedataplotcolorforlabels: "1",
    theme: "fusion"
  },
  data: [
    {
      label: "KL",
      value: "3264"
    },
    {
      label: "Selangor",
      value: "2212"
    },
    {
      label: "Lain-lain",
      value: "1436"
    }
  ]
};

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  width = '100%'
  height = '100%'
  dataFormat = "json"

  type = "msarea"
  dataSource = data

  type2 = "doughnut2d"
  dataSource2 = data2

  type3 = "pie2d"
  dataSource3 = data3
  
  constructor() { }

  ngOnInit() {
  }

}
