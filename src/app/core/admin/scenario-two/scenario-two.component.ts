import { Component, OnInit } from '@angular/core';

const data = {
  chart: {
    lowerlimit: "0",
    upperlimit: "100",
    showvalue: "1",
    numbersuffix: "%",
    theme: "fusion",
    showtooltip: "0"
  },
  colorrange: {
    color: [
      {
        minvalue: "0",
        maxvalue: "50",
        code: "#F2726F"
      },
      {
        minvalue: "50",
        maxvalue: "75",
        code: "#FFC533"
      },
      {
        minvalue: "75",
        maxvalue: "100",
        code: "#62B58F"
      }
    ]
  },
  dials: {
    dial: [
      {
        value: "81"
      }
    ]
  }
};

const dataOne = {
  chart: {
    theme: "fusion",
    numbersuffix: "",
    plotfillalpha: "40"
  },
  categories: [
    {
      category: [
        {
          label: "Jan"
        },
        {
          label: "Feb"
        },
        {
          label: "Mar"
        },
        {
          label: "Apr"
        },
        {
          label: "May"
        },
        {
          label: "Jun"
        },
        {
          label: "Jul"
        },
        {
          label: "Aug"
        },
        {
          label: "Sept"
        },
        {
          label: "Oct"
        },
        {
          label: "Nov"
        },
        {
          label: "Dec"
        }
      ]
    }
  ],
  dataset: [
    {
      seriesname: "Avg. High",
      plottooltext: "<b>$label</b> </b>",
      data: [
        {
          value: "39"
        },
        {
          value: "42"
        },
        {
          value: "50"
        },
        {
          value: "60"
        },
        {
          value: "71"
        },
        {
          value: "79"
        },
        {
          value: "85"
        },
        {
          value: "83"
        },
        {
          value: "76"
        },
        {
          value: "65"
        },
        {
          value: "54"
        },
        {
          value: "79"
        },
        {
          value: "79"
        }
      ]
    },
    {
      seriesname: "Avg. Low",
      plottooltext: "<b>$label</b> <b>$datavalue</b>",
      data: [
        {
          value: "26"
        },
        {
          value: "29"
        },
        {
          value: "35"
        },
        {
          value: "44"
        },
        {
          value: "55"
        },
        {
          value: "64"
        },
        {
          value: "70"
        },
        {
          value: "69"
        },
        {
          value: "61"
        },
        {
          value: "50"
        },
        {
          value: "41"
        },
        {
          value: "32"
        }
      ]
    }
  ]
};

const dataTwo = {
  chart: {
    showvalues: "0",
    labeldisplay: "ROTATE",
    rotatelabels: "1",
    plothighlighteffect: "fadeout",
    plottooltext: "$seriesName in $label : <b>$dataValue</b>",
    theme: "fusion"
  },
  axis: [
    {
      title: "Data One",
      titlepos: "left",
      numberprefix: "$",
      divlineisdashed: "1",
      maxvalue: "100000",
      dataset: [
        {
          seriesname: "Data 1",
          linethickness: "3",
          data: [
            {
              value: "38450.2"
            },
            {
              value: "16544.4"
            },
            {
              value: "10659.4"
            },
            {
              value: "9657.4"
            },
            {
              value: "9040.4"
            },
            {
              value: "9040.4"
            },
            {
              value: "6992.3"
            },
            {
              value: "6650.5"
            },
            {
              value: "6650.5"
            },
            {
              value: "6337.2"
            },
            {
              value: "5835.4"
            },
            {
              value: "4582.9"
            }
          ]
        }
      ]
    },
    {
      title: "Data Two",
      axisonleft: "1",
      titlepos: "left",
      numdivlines: "8",
      divlineisdashed: "1",
      maxvalue: "25",
      numbersuffix: "%",
      dataset: [
        {
          seriesname: "Data 2",
          dashed: "1",
          data: [
            {
              value: "17.23"
            },
            {
              value: "7.41"
            },
            {
              value: "4.78"
            },
            {
              value: "4.33"
            },
            {
              value: "4.05"
            },
            {
              value: "4.05"
            },
            {
              value: "3.13"
            },
            {
              value: "2.98"
            },
            {
              value: "2.98"
            },
            {
              value: "2.84"
            },
            {
              value: "2.62"
            },
            {
              value: "2.05"
            }
          ]
        }
      ]
    },
    {
      title: "Data Three",
      titlepos: "RIGHT",
      axisonleft: "0",
      numdivlines: "5",
      numbersuffix: "",
      divlineisdashed: "1",
      maxvalue: "400000",
      dataset: [
        {
          seriesname: "Data 3",
          linethickness: "3",
          data: [
            {
              value: "358196"
            },
            {
              value: "166138"
            },
            {
              value: "107288"
            },
            {
              value: "97268"
            },
            {
              value: "91098"
            },
            {
              value: "91098"
            },
            {
              value: "70617"
            },
            {
              value: "67199"
            },
            {
              value: "67199"
            },
            {
              value: "64066"
            },
            {
              value: "59048"
            },
            {
              value: "46523"
            }
          ]
        }
      ]
    },
    {
      title: "Data Four",
      titlepos: "RIGHT",
      axisonleft: "0",
      numdivlines: "5",
      divlineisdashed: "1",
      maxvalue: "20",
      numbersuffix: "%",
      dataset: [
        {
          seriesname: "Data 4",
          dashed: "1",
          data: [
            {
              value: "16.3"
            },
            {
              value: "7.03"
            },
            {
              value: "4.54"
            },
            {
              value: "4.12"
            },
            {
              value: "3.86"
            },
            {
              value: "3.86"
            },
            {
              value: "2.99"
            },
            {
              value: "2.84"
            },
            {
              value: "2.84"
            },
            {
              value: "2.71"
            },
            {
              value: "2.5"
            },
            {
              value: "1.97"
            }
          ]
        }
      ]
    }
  ],
  categories: [
    {
      category: [
        {
          label: "2006"
        },
        {
          label: "2007"
        },
        {
          label: "2008"
        },
        {
          label: "2009"
        },
        {
          label: "2010"
        },
        {
          label: "2011"
        },
        {
          label: "2012"
        },
        {
          label: "2013"
        },
        {
          label: "2014"
        },
        {
          label: "2015"
        },
        {
          label: "2016"
        },
        {
          label: "2017"
        }
      ]
    }
  ]
};


const dataThree = {
  chart: {
    dateformat: "mm/dd/yyyy",
    theme: "fusion",
    useverticalscrolling: "0"
  },
  datatable: {
    headervalign: "bottom",
    datacolumn: [
      {
        headertext: "Owner",
        headervalign: "bottom",
        headeralign: "left",
        align: "left",
        text: [
          {
            label: "Product Team"
          },
          {
            label: "Marketing Team"
          },
          {
            label: "Product Team"
          },
          {
            label: "Dev Team"
          },
          {
            label: "Design Team"
          },
          {
            label: "Dev Team"
          },
          {
            label: "QA Team"
          },
          {
            label: "Product Team"
          },
          {
            label: "Marketing Team"
          }
        ]
      }
    ]
  },
  tasks: {
    task: [
      {
        start: "1/1/2018",
        end: "1/13/2018",
        color: "#5D62B5"
      },
      {
        start: "1/4/2018",
        end: "1/21/2018",
        color: "#29C3BE"
      },
      {
        start: "1/22/2018",
        end: "2/4/2018",
        color: "#5D62B5"
      },
      {
        start: "2/5/2018",
        end: "2/11/2018",
        color: "#F2726F"
      },
      {
        start: "2/12/2018",
        end: "2/18/2018",
        color: "#FFC533"
      },
      {
        start: "2/19/2018",
        end: "3/11/2018",
        color: "#F2726F"
      },
      {
        start: "3/12/2018",
        end: "3/18/2018",
        color: "#62B58F"
      },
      {
        start: "3/16/2018",
        end: "3/23/2018",
        color: "#5D62B5"
      },
      {
        start: "3/24/2018",
        end: "3/29/2018",
        color: "#29C3BE"
      }
    ]
  },
  processes: {
    align: "left",
    headertext: "Tasks",
    headervalign: "bottom",
    headeralign: "left",
    process: [
      {
        label: "PRD & User-Stories"
      },
      {
        label: "Persona & Journey"
      },
      {
        label: "Architecture"
      },
      {
        label: "Prototyping"
      },
      {
        label: "Design"
      },
      {
        label: "Development"
      },
      {
        label: "Testing & QA"
      },
      {
        label: "UAT Test"
      },
      {
        label: "Handover & Documentation"
      }
    ]
  },
  categories: [
    {
      category: [
        {
          start: "1/1/2018",
          end: "4/1/2018",
          label: "Project Pipeline for Q1-2018"
        }
      ]
    },
    {
      category: [
        {
          start: "1/1/2018",
          end: "1/31/2018",
          label: "Jan"
        },
        {
          start: "2/1/2018",
          end: "2/28/2018",
          label: "Feb"
        },
        {
          start: "3/1/2018",
          end: "4/1/2018",
          label: "Mar"
        }
      ]
    },
    {
      category: [
        {
          start: "1/1/2018",
          end: "1/7/2018",
          label: "Week 1"
        },
        {
          start: "1/8/2018",
          end: "1/14/2018",
          label: "Week 2"
        },
        {
          start: "1/15/2018",
          end: "1/21/2018",
          label: "Week 3"
        },
        {
          start: "1/22/2018",
          end: "1/28/2018",
          label: "Week 4"
        },
        {
          start: "1/29/2018",
          end: "2/4/2018",
          label: "Week 5"
        },
        {
          start: "2/5/2018",
          end: "2/11/2018",
          label: "Week 6"
        },
        {
          start: "2/12/2018",
          end: "2/18/2018",
          label: "Week 7"
        },
        {
          start: "2/19/2018",
          end: "2/25/2018",
          label: "Week 8"
        },
        {
          start: "2/26/2018",
          end: "3/4/2018",
          label: "Week 9"
        },
        {
          start: "3/5/2018",
          end: "3/11/2018",
          label: "Week 10"
        },
        {
          start: "3/12/2018",
          end: "3/18/2018",
          label: "Week 11"
        },
        {
          start: "3/19/2018",
          end: "3/25/2018",
          label: "Week 12"
        },
        {
          start: "3/26/2018",
          end: "4/1/2018",
          label: "Week 13"
        }
      ]
    }
  ]
};

@Component({
  selector: 'app-scenario-two',
  templateUrl: './scenario-two.component.html',
  styleUrls: ['./scenario-two.component.scss']
})
export class ScenarioTwoComponent implements OnInit {

  width = '100%';
  height = '100%';
  dataFormat = "json";

  type = "angulargauge";
  dataSource = data;
  typeOne = 'radar'
  dataSourceOne = dataOne
  typeTwo = 'multiaxisline'
  dataSourceTwo = dataTwo
  typeThree = 'gantt'
  dataSourceThree = dataThree
  
  constructor() { }

  ngOnInit() {
  }

}
