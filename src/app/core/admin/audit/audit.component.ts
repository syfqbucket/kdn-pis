import { Component, OnInit } from '@angular/core';


const data = {
  chart: {
    subcaption: "Scale: 1 (low) to 5 (high)",
    theme: "fusion",
    showlegend: "0",
    showdivlinevalues: "0",
    showlimits: "0",
    showvalues: "1",
    plotfillalpha: "40",
    plottooltext: " <b>$label</b>  <b>$value</b>"
  },
  categories: [
    {
      category: [
        {
          label: "Communication"
        },
        {
          label: "Punctuality"
        },
        {
          label: "Problem Solving"
        },
        {
          label: "Meeting Deadlines"
        },
        {
          label: "Team Player"
        },
        {
          label: "Technical Knowledge"
        }
      ]
    }
  ],
  dataset: [
    {
      seriesname: "User Ratings",
      data: [
        {
          value: "3"
        },
        {
          value: "3"
        },
        {
          value: "4"
        },
        {
          value: "3"
        },
        {
          value: "2"
        },
        {
          value: "4"
        }
      ]
    }
  ]
};

@Component({
  selector: 'app-audit',
  templateUrl: './audit.component.html',
  styleUrls: ['./audit.component.scss']
})
export class AuditComponent implements OnInit {

  width = '100%';
  height = '100%';
  type = "radar";
  dataFormat = "json";
  dataSource = data;

  constructor() { }

  ngOnInit() {
  }

}
