export const PengurusanData = [{
    "id": "STF27300",
    "name": "Ashleigh Aldwick",
    "email": "aaldwick0@upenn.edu",
    "role": "Pengguna",
    "date": "07/09/2019"
  }, {
    "id": "STF87598",
    "name": "Horton Van Leijs",
    "email": "hvan1@newsvine.com",
    "role": "Admin",
    "date": "01/05/2019"
  }, {
    "id": "STF32275",
    "name": "Wylie Puffett",
    "email": "wpuffett2@gov.uk",
    "role": "Jururunding",
    "date": "21/04/2019"
  }, {
    "id": "STF57969",
    "name": "Eugenio Acland",
    "email": "eacland3@barnesandnoble.com",
    "role": "Pengguna",
    "date": "18/04/2019"
  }, {
    "id": "STF45195",
    "name": "Madelaine Forten",
    "email": "mforten4@theatlantic.com",
    "role": "Admin",
    "date": "17/08/2019"
  }, {
    "id": "STF94360",
    "name": "Suzy Battelle",
    "email": "sbattelle5@joomla.org",
    "role": "Jururunding",
    "date": "06/09/2019"
  }, {
    "id": "STF67682",
    "name": "Sandie Mattiazzo",
    "email": "smattiazzo6@behance.net",
    "role": "Jururunding",
    "date": "30/06/2019"
  }, {
    "id": "STF93051",
    "name": "Roosevelt Claussen",
    "email": "rclaussen7@dion.ne.jp",
    "role": "Pengguna",
    "date": "14/12/2018"
  }, {
    "id": "STF20030",
    "name": "Kania Minton",
    "email": "kminton8@google.fr",
    "role": "Admin",
    "date": "21/09/2019"
  }, {
    "id": "STF66142",
    "name": "Shaun Francis",
    "email": "sfrancis9@rakuten.co.jp",
    "role": "Pengguna",
    "date": "25/10/2018"
  }]