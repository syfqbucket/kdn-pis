import { Component, OnInit } from '@angular/core';
import { PengurusanData } from './data';

@Component({
  selector: 'app-administrator',
  templateUrl: './administrator.component.html',
  styleUrls: ['./administrator.component.scss']
})
export class AdministratorComponent implements OnInit {

  public loadedData = PengurusanData
  
  constructor() { }

  ngOnInit() {
  }

}
