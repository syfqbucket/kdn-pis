import { Component, OnInit } from '@angular/core';
import { Deck } from '@deck.gl/core';
import { LineLayer } from '@deck.gl/layers';
import { Map, SymbolLayout } from 'mapbox-gl';
import {ScatterplotLayer} from '@deck.gl/layers';

const scatterplotLayer = new ScatterplotLayer({
  id: 'bart-stations',
  data: 'https://github.com/uber-common/deck.gl-data/blob/master/website/bart-stations.json',
  getRadius: d => Math.sqrt(d.entries) / 100,
  getPosition: d => d.coordinates,
  getFillColor: [255, 228, 0],
});

@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  styleUrls: ['./trends.component.scss']
})
export class TrendsComponent implements OnInit {

  testLayer = scatterplotLayer

  mapboxStyle = 'mapbox://styles/mapbox/light-v9'
  mapboxZoom = 15.5
  mapboxCenter = [101.7136,3.1489]
  mapboxPitch = 45
  mapboxBearing = -17.6

  mapboxLayerFilter = ['==', 'extrude', 'true']
  mapboxLayerMinZoom = 15
  mapboxLayerPaint = {
    'fill-extrusion-color': '#aaa',
    'fill-extrusion-height': [
      'interpolate',
      ['linear'],
      ['zoom'],
      15, 0,
      15.05,
      ['get', 'height']
    ],
    'fill-extrusion-base': [
      'interpolate',
      ['linear'],
      ['zoom'],
      15, 0,
      15.05,
      [
        'get',
        'min_height'
      ]
    ],
    'fill-extrusion-opacity': .6
  }

  labelLayerId: string;


  onLoad(mapInstance: Map) {
    const layers = mapInstance.getStyle().layers!;

    for (let i = 0; i < layers.length; i++) {
      if (layers[i].type === 'symbol' && (<SymbolLayout>layers[i].layout)['text-field']) {
        this.labelLayerId = layers[i].id;
        break;
      }
    }
  }

  constructor() { }

  ngOnInit() {
  }

}
