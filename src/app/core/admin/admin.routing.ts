import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ToolsComponent } from './tools/tools.component';
import { CrimeComponent } from './crime/crime.component';
import { HotspotComponent } from './hotspot/hotspot.component';
import { TrendsComponent } from './trends/trends.component';
import { ScenarioOneComponent } from './scenario-one/scenario-one.component';
import { ScenarioTwoComponent } from './scenario-two/scenario-two.component';
import { ReportComponent } from './report/report.component';
import { AdministratorComponent } from './administrator/administrator.component';
import { AuditComponent } from './audit/audit.component';
import { AnalyticsComponent } from './analytics/analytics.component';

export const AdminRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent
            },
            {
                path: 'tools',
                component: ToolsComponent
            },
            {
                path: 'crime',
                component: CrimeComponent
            },
            {
                path: 'hotspot',
                component: HotspotComponent
            },
            {
                path: 'trends',
                component: TrendsComponent
            },
            {
                path: 'scenario-one',
                component: ScenarioOneComponent
            },
            {
                path: 'scenario-two',
                component: ScenarioTwoComponent
            },
            {
                path: 'report',
                component: ReportComponent
            },
            {
                path: 'administrator',
                component: AdministratorComponent
            },
            {
                path: 'audit',
                component: AuditComponent
            },
            {
                path: 'analytics',
                component: AnalyticsComponent
            }
        ]
    }
]