export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    collapse?: string;
    isCollapsed?: boolean;
    isCollapsing?: any;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    type?: string;
    collapse?: string;
    children?: ChildrenItems2[];
    isCollapsed?: boolean;
}
export interface ChildrenItems2 {
    path?: string;
    title?: string;
    type?: string;
}
//Menu Items
export const ROUTES: RouteInfo[] = [
  {
    path: "/admin/dashboard",
    title: "Dashboard",
    type: "link",
    icontype: "fas fa-desktop text-warning"
  },
  {
    path: '/admin/tools',
    title: 'Tools',
    type: 'link',
    icontype: 'fas fa-desktop text-blue'
  },
  {
    path: '/admin/crime',
    title: 'Crime',
    type: 'link',
    icontype: 'fas fa-desktop text-pink'
  },
  {
    path: '/admin/hotspot',
    title: 'Hotspot',
    type: 'link',
    icontype: 'fas fa-desktop text-red'
  },
  {
    path: '/admin/trends',
    title: 'Trends',
    type: 'link',
    icontype: 'fas fa-desktop text-yellow'
  },
  {
    path: '/admin/scenario-one',
    title: 'Scenario One',
    type: 'link',
    icontype: 'fas fa-desktop text-green'
  },
  {
    path: '/admin/scenario-two',
    title: 'Scenario Two',
    type: 'link',
    icontype: 'fas fa-desktop text-blue'
  },
  {
    path: '/admin/report',
    title: 'Report',
    type: 'link',
    icontype: 'fas fa-desktop text-red'
  },
  {
    path: '/admin/administrator',
    title: 'Administrator',
    type: 'link',
    icontype: 'fas fa-desktop text-orange'
  },
  {
    path: '/admin/audit',
    title: 'Audit',
    type: 'link',
    icontype: 'fas fa-desktop text-purple'
  },
  {
    path: '/admin/analytics',
    title: ' Analytics',
    type: 'link',
    icontype: 'fas fa-desktop text-pink'
  }
];
